import markovify

class BotElMarkovify():
    """docstring for BotElMarkovify"""
    def __init__(self):
        self.leodict = self.gtext("leo.txt")
        self.kaidict = self.gtext("kai.txt")
        self.arctrekdict = self.gtext("arctrek.txt")
        self.sherlockdict = self.gtext("sherlock.txt")
        self.dicts = {"leo":self.leodict,"kai":self.kaidict,"arctrek":self.arctrekdict,"sherlock":self.sherlockdict}

    def buildDict(self, words):
        """
        Build a dictionary from the words.
        """
        text_model = {}
        text_model = markovify.Text(words)
        return text_model

    def gtext(self, fname):
        """ Gets text from a local file, passes it to build_dict().
        """
        with open(fname, "rt", encoding="utf-8") as f:
            text = f.read()
        d = self.buildDict(text)
        return d

    def generateSentence(self, dictionary):
        """ Creates a sentence out of the word chunks in the input dictionary.
        """
        word_list = []
        for i in range(5):
            word_list.append(dictionary.make_sentence())
        return ' '.join(word_list)

    def getChain(self, corpus, requester, leader=None):
        """ Exposes the chaining mechanism to BotEl.
        """
        if leader == None: leader = "\""
        print("{} requested a markovified chain for {}.".format(requester, corpus))
        if corpus in self.dicts.keys():
            output = "{}".format(leader)
            output += self.generateSentence(self.dicts[corpus])
        else:
            output = "{}Sorry {}, {} isn't one of the texts I know.".format(leader, requester, corpus)
        return output